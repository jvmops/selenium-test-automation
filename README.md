
# Selenium test automation

## Brief

Before you will run your first test you will need to install some stuff.
Lets bullet list things you will need to have.
- [java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
  \- download only **Java SE Development Kit** - you don't need demos; after installing it remember about setting your JAVA_HOME system variable  
- [git bash emulator](https://gitforwindows.org/) - you need this to
  emulate linux terminal on your windows machine (windows terminal use
  different commands)
- [intellij community edition](https://www.jetbrains.com/idea/download)
  \- notice license - **Apache 2.0**. It means open source and you can
  use it freely. Selenium WebDriver is under the same license too

In order to confirm your installation you will need to type following commands into your **linux terminal emulator - bash**. You will run this emulator by a file with .sh extension. If you notice some wierd errors you probably opened git terminal with an .exe file which is a **CMD - windows terminal**, not a linux one. It has it's own set of commands.

```bash
# some nice linux commands that you can use in order to confirm your terminal is ready to go
$ ls - this will display files in the current directory
$ pwd - this will display absolute path of the current directory 
$ cd - this will change directory to the one you provide
```
```bash
# to confirm java installation first check if the environment variable is set properly 
$ echo $JAVA_HOME
/somewhere/in/your/harddrive/where/the/java/is

# remember to add $JAVA_HOME/bin to your windows PATH - you can check PATH with:
$ echo $PATH

# next check your Java version
$ java -version
java version "1.8.0_xxx"
Java(TM) SE Runtime Environment (build 1.8.0_xxx)
Java HotSpot(TM) 64-Bit Server VM (build xx.xx-xx, mixed mode)
```

When you got all those things installed and ready to go, request an
access to this repository. In order to do that you will need a gitlab
account. Please attach to it.