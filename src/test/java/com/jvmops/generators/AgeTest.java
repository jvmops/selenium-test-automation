package com.jvmops.generators;

import com.jvmops.generators.human.Age;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AgeTest {

    @Autowired
    private Age age;

    @Test
    public void generated_age_is_not_null() {
        Assert.notNull(age.get(), "generatedAge is not null");
    }


    /**
     * Może i by to miało sens gdyby powtórzyć ten test z 500 razy, ale JUnit nie umożliwia tego w tej wersji.
     *      Starałbym się jednak unikać pisania testów, które potrzebują tej funkcjonalności. Z tego też powodu
     *      test ten jest zignorowany i tutaj też pozostawiony.
     */
    @Test
    @Ignore
    public void age_fits_in_given_range() {
        Integer randomAge = age.get();
        Assert.isTrue((randomAge > 0) && (randomAge <151), "generated age is out of bounds");
    }
}
