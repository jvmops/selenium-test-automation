package com.jvmops.generators;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomClassAcceptanceTest {
    // stałe dla testu
    private static final Random RANDOM = new Random();
    private static final int UPPER_BOUND_EXCLUSIVE = 2;
    private static final List<String> LETTERS = Arrays.asList("a", "b", "c");
    private static final int C_INDEX = 2;
    private static final String UPPER_BOUND_ERROR_MESSAGE = String.format("Number should be less that %s", UPPER_BOUND_EXCLUSIVE);

    @Test
    public void random_can_generate_0() {
        // wejść do metody nextInt(...) żeby przypomnieć sobie jak to wygląda w środku
        Assert.assertEquals(0, RANDOM.nextInt(1));
    }

    @Test
    public void random_can_generate_numbers_LESS_than_UPPER_BOUND() {
        int generatedNumber = RANDOM.nextInt(UPPER_BOUND_EXCLUSIVE);
        Assert.assertTrue(UPPER_BOUND_ERROR_MESSAGE, generatedNumber < UPPER_BOUND_EXCLUSIVE);
    }

    @Test
    public void list_know_its_size() {
        Assert.assertEquals(3, LETTERS.size());
    }

    @Test
    public void you_can_get_an_element_from_list_by_index() {
        String thirdLetter = LETTERS.get(C_INDEX);
        Assert.assertEquals("c", thirdLetter);
    }

    @Test
    public void you_can_use_random_class_to_retrieve_random_element_from_list() {
        int randomIndex = RANDOM.nextInt(LETTERS.size());
        String letter = LETTERS.get(randomIndex);
        Assert.assertNotNull(letter);
        Assert.assertTrue("litera jest niepusta", !letter.isEmpty());
    }
}
