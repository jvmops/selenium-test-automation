package com.jvmops.generators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HumanFactoryTest {

    @Autowired
    private HumanFactory human;

    @Test
    public void HumanGenerator_can_generate_valid_human() {
        Human randomHuman = human.get();
        Assert.notNull(randomHuman.getName(), "name == null");
        Assert.notNull(randomHuman.getAge(), "age == null");
    }
}

