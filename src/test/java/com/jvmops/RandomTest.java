package com.jvmops;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomTest {
    Random random = new Random();

    List<String> imiona = Arrays.asList("Marta", "Dominika", "Krystian", "Magda", "Emilia", "Sebastian", "Paweł");


    @Test
    public void zwroc_imie_z_listy() {
        String imie = imiona.get(1);
        Assert.assertEquals("Dominika", imie);
    }
    @Test
    public void zwroc_randomowe_imie_z_listy() {
        String imie = imiona.get(random.nextInt(imiona.size()));
        Assert.assertTrue("imie jest puste", !imie.isEmpty());
    }
}
