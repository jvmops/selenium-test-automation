package com.jvmops.headfirst.magda.bard;

import com.jvmops.generators.Human;
import org.junit.Assert;
import org.junit.Test;

/**
 * Very simple test. It
 */
public class HumanTest {
    private static final String NAME = "Sebastian";
    private static final Integer AGE = 777;

    Human sebastian = new Human(NAME, AGE);

    @Test
    public void human_needs_to_know_his_name() {
        Assert.assertEquals(NAME, sebastian.getName());
    }

    @Test
    public void human_needs_to_know_his_age() {
        Assert.assertEquals(AGE, sebastian.getAge());
    }
}
