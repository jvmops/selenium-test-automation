package com.jvmops.headfirst.magda.bard;

import org.junit.Assert;
import org.junit.Test;

public class BardObjectOrientedTest {

    BardObjectOriented bard = new BardObjectOriented();

    @Test(expected = IllegalStateException.class)
    public void you_can_not_have_negative_number_of_beers_on_the_wall() {
        bard.singAboutBeersOnTheWall(-2);
    }

    @Test
    public void when_singing_about_multiple_beers_bard_will_use_word_beers_1() {
        String song = bard.singAboutBeersOnTheWall(55);
        Assert.assertTrue("Song does not contain word: beers", song.contains("beers"));
    }

    @Test
    public void when_singing_about_multiple_beers_bard_will_use_word_beers_2() {
        String song = bard.singAboutBeersOnTheWall(2);
        Assert.assertTrue("Song does not contain word: beers", song.contains("beers"));
    }

    @Test
    public void when_singing_about_multiple_beers_bard_will_sing_3_verses() {
        String song = bard.singAboutBeersOnTheWall(33);
        Assert.assertEquals(3, song.split(System.lineSeparator()).length);
    }

    @Test
    public void when_singing_about_one_beer_bard_should_use_word_beer() {
        String song = bard.singAboutBeersOnTheWall(1);
        Assert.assertTrue("Song does not contain word: beer", song.contains("beer"));
    }

    @Test
    public void when_the_wall_is_empty_bard_will_complain() {
        String song = bard.singAboutBeersOnTheWall(0);
        Assert.assertEquals(1, song.split(System.lineSeparator()).length);
    }
}
