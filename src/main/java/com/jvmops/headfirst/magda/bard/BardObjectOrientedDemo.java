package com.jvmops.headfirst.magda.bard;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * declarative vs imprative style in programming
 * - sql
 * - java different ways
 * - different languages same shit
 * -
 */
class BardObjectOrientedDemo {
//    TODO: bardzo ważne jest skumanie co oznacza static i final zarowno w kontekscie pola klasy jak i metody - musimy sobie o tym poszukać wiedzy w google
    private static final int SEED = 99; // static i final w kontekście pola klasy oznacza stałą - tutaj wybieramy startową liczbę piw rozstawionych na ścianie
    private static final BardObjectOriented bard = new BardObjectOriented(); // tutaj tworzymy obiekt klasy BardObjectOriented z wykorzystaniem domyślnego konstruktora

    public static void main(String[] args) {
//        Dla ułatwienia demonstracji utworzyłem dwie funkcje poniżej. Jedna z tych funkcji tworzy piosenkę przy pomocy pętli while.
        whileLoopSong(); // wywołanie funkcji, która nic nie zwraca - spodziewam się dostać piosenkę wydrukowaną przez program w konsoli

//        Druga funkcja używa nowoczesnej biblioteki oferującej strumieniowanie danych - to jest to z czego słynie Java 8. Po zapoznaniu i uruchomieniu kodu w pętli while, zakomentuj metodę whileLoopSong() i odkomentuj poniższą linijkę za pomocą: "ctrl + /". Spodziewaj się przy tym takiego samego rezultatu w konsoli. Polecam też włączyć funkcję soft-wrap przy pomocy skrótu "ctrl + alt + a", tam wpisujemy soft-wrap i przełączamy tą wajchę dla wszystkich plików.
//        imperativeStyleStreamSong();
    }

    /**
     * declarative way of programming
     */
    private static void whileLoopSong() {
//        deklarujemy zmienną seed na potrzeby pętli while - seed staje się naszym licznikiem butelek na ścianie
//        przy deklaracji zmiennej używamy stałej SEED, aby móc łatwo wszędzie (np. w metodzie java8StreamSong poniżej) sterować startową ilością butelek
        int currentBottleNumber = SEED;
//        jeśli tworzymy String'a poprzez pętlę, jak np. poprzez pętlę while poniżej, to musimy do tego używać StringBuilder'a - Java tego wymaga!
        StringBuilder song = new StringBuilder();
        while (currentBottleNumber >= 0) { // dopóki liczba butelek na ścianie nie spadnie do 0
//            prosimy barda o zaśpiewanie piosenki
//            ilość piw policzyliśmy sami, bo to bard te browary spija i nie można mu ufać w rachunkach
            String verses = bard.singAboutBeersOnTheWall(currentBottleNumber);
            song.append(verses); // dodajemy wersy piosenki do StringBuilder'a
            currentBottleNumber--; // bard wziął piwo za piosenkę, więc odejmujemy je od licznika
        }
//        tutaj tworzymy String'a z piosenką - na obiekcie klasy StringBuilder wołamy metodę .toString() i trochę na wyrost przypisujemy wynik do zmiennej songToBePrinted
        String songToBePrinted = song.toString();
//        przekazujemy zmienną songToBePrinted do metody - można też zrobić od razu tak: System.out.println(song.toString());
        System.out.println(songToBePrinted);
    }

    /**
     * imperative way of programming
     */
    private static void java8StreamSong() {
        // tutaj widzimy że stała SEED pomaga nam uzyskać tą samą ilość startową butelek na ścianie
        String song = Stream.iterate(SEED, currentBottleNumber -> currentBottleNumber - 1) // utwórz ciąg liczb, zacznij od 99 odejmować 1
                .limit(SEED + 1) // ciąg liczb musi być o jeden większy od początku zakresu, aby uwzględnić 0 dla piosenki barda
                .map(bard::singAboutBeersOnTheWall) // tutaj wołamy barda, żeby zaśpiewał piosenkę dla każdej liczby z ciągu (nasza liczba butelek)
                .collect(Collectors.joining()); // a tutaj łączymy wszystkie wersy w String'a
        System.out.println(song); // drugowanie piosenki na konsoli
    }
}
