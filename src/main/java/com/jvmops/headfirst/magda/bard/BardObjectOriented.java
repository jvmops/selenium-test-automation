package com.jvmops.headfirst.magda.bard;

/**
 * Klasa ta utworzona została, aby zebrać logikę śpiewania piosenki w jednym miejscu.
 *
 * Wydzielenie tej logiki do osobnej klasy, pomogło nam bardzo w poprawie czytelności i utrzymywalności kodu. Wykorzystanie stałych pod nazwy szablonow oraz użycie metody {@link String#format(String, Object...)} sprawiło, że kod stał się bardziej "czysty" :D Dzięki temu moglismy nazwac nasze meetody pomocnicze (prywatne), Co za tym idzie nasze metody domenowe (pakietowe) latwiej sie czyta.
 *
 * Nad metodami domenowymi Bard'a powstały testy jednostkowe - znajdują się one w odpowiednim pakiecie w katologu ./selenium-test-automation/src/test
 */
class BardObjectOriented {
    private static final String LONG_VERSE = "There are %s %s on the wall";
    private static final String SHORT_VERSE = "There are %s %s";
    private static final String PICKED_BEER = "I just picked another one";

    String singAboutBeersOnTheWall(Integer beersCount) {
        if (beersCount < 0) {
            throw new IllegalStateException("Beer count can not be lower than 0");
        }

        if (beersCount == 0) {
            return "No more beers on the wall";
        }

        String beersLiteral = "beers";
        if (beersCount == 1) {
            beersLiteral = "beer";
        }

        return longVerse(beersCount, beersLiteral) +
                shortVerse(beersCount, beersLiteral) +
                PICKED_BEER + System.lineSeparator();
    }

    // TODO: implement this method asap, business really needs it
    Song createSongAboutBeersOnTheWall(Integer beersCount) {
        throw new IllegalStateException("Not implemented yet");
    }

    private String shortVerse(Integer beersCount, String beers) {
        return String.format(SHORT_VERSE, beersCount, beers) + System.lineSeparator();
    }

    private String longVerse(Integer beersCount, String beers) {
        return String.format(LONG_VERSE, beersCount, beers) + System.lineSeparator();
    }
}
