package com.jvmops.generators.abstractions;

import java.util.Random;

/**
 * Z dziedziczenia po tym typie powinny skorzystać wszystkie klasy, które potrzebują Random'a.
 */
public abstract class RandomBasedGenerator {
    protected static final Random RANDOM = new Random();
}
