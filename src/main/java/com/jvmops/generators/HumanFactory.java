package com.jvmops.generators;

import com.jvmops.generators.human.Age;
import com.jvmops.generators.human.Name;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Fabryka obiektów domenowych
 */
@Component
public class HumanFactory {
    private Name name;
    private Age age;

    @Autowired
    public HumanFactory(Name name, Age age) {
        this.name = name;
        this.age = age;
    }

    public Human get() {
        return new Human(
                name.get(),
                age.get()
        );
    }
}
