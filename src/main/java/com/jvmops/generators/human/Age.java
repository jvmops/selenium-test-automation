package com.jvmops.generators.human;

import com.jvmops.generators.abstractions.RandomBasedGenerator;
import org.springframework.stereotype.Component;

@Component
public class Age extends RandomBasedGenerator {
    private final static Integer MAX_AGE = 151;

    public Integer get() {
        return RANDOM.nextInt(MAX_AGE);
    }
}
