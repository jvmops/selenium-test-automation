package com.jvmops.generators.human;

import com.jvmops.generators.abstractions.RandomBasedGenerator;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class Name extends RandomBasedGenerator {
    private static final List<String> names = Arrays.asList("Janusz", "Mirosław", "Janina", "Jan");

    public String get() {
        int randomNameIndex = RANDOM.nextInt(names.size());
        return names.get(randomNameIndex);
    }
}
